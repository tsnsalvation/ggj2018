﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable AI/Action/Mill")]
public class MillAction : Action {

    public override void Act(StateController stateController)
    {
        stateController.mill = true;
        stateController.navAgent.enabled = false;
    }
}
