﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable AI/Action/Wander")]
public class WanderAction : Action {

    public override void Act(StateController stateController)
    {
        stateController.navAgent.enabled = true;
        stateController.mill = false;
        Wander(stateController);
    }

    public void Wander(StateController stateController)
    {
        stateController.navAgent.destination = stateController.currentIntersectionTarget.transform.position;
        stateController.navAgent.isStopped = false;

        if (Vector3.Distance(stateController.transform.position, stateController.currentIntersectionTarget.transform.position) < stateController.currentIntersectionTarget.radius)
        {
            if (stateController.currentIntersectionTarget.connectedIntersections.Length == 1)
            {
                stateController.lastIntersection = stateController.currentIntersectionTarget;
                stateController.currentIntersectionTarget = stateController.currentIntersectionTarget.connectedIntersections[0];
            }
            else
            {
                int randomIndex = Random.Range(0, stateController.currentIntersectionTarget.connectedIntersections.Length);
                if (stateController.currentIntersectionTarget.connectedIntersections[randomIndex] != stateController.lastIntersection)
                {
                    stateController.lastIntersection = stateController.currentIntersectionTarget;
                    stateController.currentIntersectionTarget = stateController.currentIntersectionTarget.connectedIntersections[randomIndex];
                }
                else
                {
                    for (int i = 0; i < stateController.currentIntersectionTarget.connectedIntersections.Length; i++)
                    {
                        if (stateController.currentIntersectionTarget.connectedIntersections[i] != stateController.lastIntersection)
                        {
                            stateController.lastIntersection = stateController.currentIntersectionTarget;
                            stateController.currentIntersectionTarget = stateController.currentIntersectionTarget.connectedIntersections[i];
                            i = stateController.currentIntersectionTarget.connectedIntersections.Length;
                        }
                    }
                }
            }
        }
    }
}
