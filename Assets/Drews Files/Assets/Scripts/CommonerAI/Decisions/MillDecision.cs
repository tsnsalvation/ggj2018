﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable AI/Decision/Mill")]
public class MillDecision : Decision {
    float timer = 0;
    public override bool Decide(StateController stateController)
    {
        if (!stateController.millOnCooldown)
        {
            if (Vector3.Distance(stateController.transform.position, stateController.lastIntersection.transform.position) < stateController.lastIntersection.radius)
            {
                if (stateController.lastIntersection.gatherArea)
                {
                    return true;
                }
            }
        }
        else
        {
            if (timer > stateController.millCooldownDuration)
            {
                stateController.millOnCooldown = false;
                timer = 0;
            }
            else
                timer += Time.deltaTime;
        }
        return false;
    }
}
