﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable AI/Decision/Return To Wander")]
public class ReturnWanderDecision : Decision {

    float timer = 0;
    public override bool Decide(StateController stateController)
    {
        timer += Time.deltaTime;
        if (timer > stateController.gatheringAreaMillDuration)
        {
            timer = 0;
            stateController.millOnCooldown = true;
            return true;
        }
        else
        {
            return false;
        }
    }
}
