﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Rigidbody))]
/*
 * Attached to the Minion Object. Is responsible for holding persistent data required for
 * decisions.
 */
public class StateController : MonoBehaviour {

    public State currentState;
    public State remainState;

    public Settings settings;

    [HideInInspector] public bool mill;
    public float gatheringAreaMillDuration;
    public bool millOnCooldown;
    public float millCooldownDuration;

    public Material dbgWalk, dbgTalk, dbgGroup;

    [HideInInspector] public NavMeshAgent navAgent;
    [HideInInspector] public Rigidbody rb;
    [HideInInspector] public Selectable selectable;

    public Intersection lastIntersection;
    public Intersection currentIntersectionTarget;
    [HideInInspector] public bool aiActive;  

    public Color debugGizmoColor;
    

    public void Awake()
    {
        if (GetComponent<NavMeshAgent>() == null)
            throw new UnassignedReferenceException();
        else
            navAgent = GetComponent<NavMeshAgent>();
        if (GetComponent<Rigidbody>() == null)
            throw new UnassignedReferenceException();
        else
            rb = GetComponent<Rigidbody>();
    }

    public void Start()
    {
        aiActive = true;
        Intersection[] intersections;
        intersections = GameObject.FindObjectsOfType<Intersection>();
        if (intersections.Length > 0)
        {
            Intersection nearest = intersections[0];
            for (int i = 1; i < intersections.Length; i++)
            {
                if (Vector3.Distance(transform.position, intersections[i].transform.position) < Vector3.Distance(transform.position, nearest.transform.position))
                {
                    nearest = intersections[i];
                }
            }
            currentIntersectionTarget = nearest;
            lastIntersection = nearest;
        }
        else
            throw new UnityException("No connected Intersections");
    }
    public void Update()
    {
        if (!aiActive)
        {
            return;
        }
        else
        {
            currentState.UpdateState(this);
        }
    }

    public void TransitionToState(State nextState)
    {
        if (nextState != remainState)
        {
            currentState = nextState;
        }
    }

    private void OnDrawGizmos()
    {
        if (currentState != null)
        {
            Gizmos.color = debugGizmoColor;
            Gizmos.DrawWireSphere(transform.position, 1);
        }
    }
}
