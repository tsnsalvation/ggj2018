﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intersection : MonoBehaviour {

    public Intersection[] connectedIntersections;
    public float radius;
    public bool gatherArea;

    private void OnDrawGizmos()
    {
        if (gatherArea)
            Gizmos.color = Color.blue;
        else
            Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
