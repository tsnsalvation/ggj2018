﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseFollow : MonoBehaviour {

    Camera playerCam;
    public LayerMask targetLayers;
    public GameObject worldSpaceMouseCursorPosition;
    public Projector projector;
    public float circleRadius = 1;

    private void Awake()
    {
        if (GetComponent<Camera>() == null)
            throw new UnassignedReferenceException();          
        else
            playerCam = GetComponent<Camera>();
    }
    private void Update()
    {
        projector.orthographicSize = circleRadius;
        RaycastHit hit;
        Physics.Raycast(playerCam.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, targetLayers);
        if (hit.collider != null)
            worldSpaceMouseCursorPosition.transform.position = hit.point;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(worldSpaceMouseCursorPosition.transform.position, circleRadius);
    }
}
