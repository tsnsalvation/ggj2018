using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeliefIndicator : MonoBehaviour
{

    //[Range(-100, 100)]
    //public int beliefLevel;

	private Minion_Belief minionBelief;

    int lastBeliefLevel;

	public bool visible = false;
    public Color neutralColor;
    public Color heresyColor;
    public Color pietyColor;
    public GameObject imagePlane;
    Material indicatorMaterial;
    public Texture hereticalTexture;
    public Texture hereticalEmissionTexture;
    public Texture piousTexture;
    public float startScale = 1;

    bool coroutineRunning;

    private void Awake()
    {
        if (imagePlane.GetComponent<Renderer>() == null)
            throw new UnassignedReferenceException();
        else
        {
            indicatorMaterial = new Material(imagePlane.GetComponent<Renderer>().material);
            imagePlane.GetComponent<Renderer>().material = indicatorMaterial;
			minionBelief = GetComponentInParent<Minion_Belief> ();
        }
    }

	void LateUpdate ()
	{
		transform.rotation = Quaternion.LookRotation (Vector3.forward, Vector3.up);
	}

    private void Update()
    {
		if ((minionBelief.belief < 0) && (hereticalTexture != null))
        {
            indicatorMaterial.SetTexture("_MainTex", hereticalTexture);
            indicatorMaterial.SetTexture("_EmissionMap", hereticalEmissionTexture);
            indicatorMaterial.SetColor("_EmissionColor", Color.red);
        }
        else
        {
			if ((minionBelief.belief > 0) && (hereticalTexture != null))
            {
                indicatorMaterial.SetTexture("_MainTex", piousTexture);
                indicatorMaterial.SetTexture("_EmissionMap", piousTexture);
                indicatorMaterial.SetColor("_EmissionColor", Color.black);

            }
        }

		if (visible) {
			float beliefAbsToScale = Mathf.Abs (minionBelief.belief);
			Vector3 targetScale = new Vector3 (beliefAbsToScale, 1f, beliefAbsToScale);
			imagePlane.transform.localScale = Vector3.Lerp (imagePlane.transform.localScale, targetScale, 0.1f);
			//imagePlane.transform.localScale = new Vector3 (Mathf.Lerp (imagePlane.transform.localScale.x, (Mathf.Abs (minionBelief.belief * 1.0f) / 100) * startScale, 0.1f), imagePlane.transform.localScale.y, Mathf.Lerp (imagePlane.transform.localScale.z, (Mathf.Abs (minionBelief.belief * 1.0f) / 100) * startScale, 0.1f));
		} else {
			imagePlane.transform.localScale = Vector3.Lerp (imagePlane.transform.localScale, Vector3.zero, 0.1f);
		}


		if (lastBeliefLevel != minionBelief.belief)
        {
            //if (!coroutineRunning)
                //StartCoroutine(colorLerp());
        }
        //if (beliefLevel == 0)
            //indicatorMaterial.SetColor("_Color", neutralColor);

    }

    IEnumerator colorLerp()
    {/*
        coroutineRunning = true;
		int diff = minionBelief.belief - lastBeliefLevel;
        if (diff > 0)
        {
            for (int i = diff; i > 0; i--)
            {
				if (minionBelief.belief <= 0)
                    indicatorMaterial.SetColor("_Color", Color.Lerp(indicatorMaterial.color, neutralColor, 0.01f));
                else
                    indicatorMaterial.SetColor("_Color", Color.Lerp(indicatorMaterial.color, pietyColor, 0.01f));
            }
			lastBeliefLevel = minionBelief.belief;
        }
        else
        {
            if (diff < 0)
            {
                for (int i = diff; i < 0; i++)
                {
					if (minionBelief.belief >= 0)
                        indicatorMaterial.SetColor("_Color", Color.Lerp(indicatorMaterial.color, neutralColor, 0.05f));
                    else
                        indicatorMaterial.SetColor("_Color", Color.Lerp(indicatorMaterial.color, heresyColor, 0.05f));
                }
				lastBeliefLevel = minionBelief.belief;
            }
        }
        coroutineRunning = false;*/
        yield return null;
    }
}

