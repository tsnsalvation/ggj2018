﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable AI/State")]
public class State : ScriptableObject {

    public Transition[] transitions;
    public Action[] actions;

    public void UpdateState(StateController stateController)
    {
        DoActions(stateController);
        CheckTransitions(stateController);
    }
	
	public void DoActions(StateController stateController)
    {
        for (int i = 0; i < actions.Length; i++)
        {
            actions[i].Act(stateController);
        }
    }

    public void CheckTransitions(StateController stateController)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            bool decisionSuccess = transitions[i].decision.Decide(stateController);

            if (decisionSuccess)
            {
                stateController.TransitionToState(transitions[i].trueState);
            }
            else
            {
                stateController.TransitionToState(transitions[i].falseState);
            }
        }
    }
}
