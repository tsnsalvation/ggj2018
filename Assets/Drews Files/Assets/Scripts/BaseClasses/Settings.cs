﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pluggable AI/Configurable Settings")]
public class Settings : ScriptableObject {
    
    [System.Serializable]
    public class MillSettings
    {
        public float gatheringAreaMillDuration = 5;
        public float millCooldownDuration = 5;
    }
    public MillSettings millSettings;
}
