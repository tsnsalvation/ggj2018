﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CrossManager : MonoBehaviour {

   public RectTransform Cross;

    // slider objects
    public Slider Faithslide;
    public Slider Fearslide;
    public Slider Hopelide;
    public Slider Heresyslide;
    public Slider Pietyyslide;

    // faith value
    [Range(0,100)]
    public float FaithVal;
    [Range(0, 100)]
    public float FearVal;
    [Range(-100, 100)]
    public float HopeVal = 0;
    [Range(-100, 100)]
    public float HeresyVal = 0;


   

    RectTransform hoperect;
    RectTransform fearrect;
    // Use this for initialization
    void Start () {
        fearrect = Fearslide.GetComponent<RectTransform>();
        hoperect = Hopelide.GetComponent<RectTransform>();
        
		
	}
	
	// Update is called once per frame
	void Update () {
        // assing bar values to to represent in sliders
        Faithslide.value = (FaithVal / 100);
        Hopelide.value = Mathf.Clamp((HopeVal / 100),0,100);
        Fearslide.value = -1* (Mathf.Clamp((HopeVal / 100), -100, 0));
        Pietyyslide.value = Mathf.Clamp((HeresyVal / 100), 0, 100);
        Heresyslide.value = -1 * (Mathf.Clamp((HeresyVal / 100), -100, 0));
        

        CheckStatus();
        
        
    }

    void CheckStatus()
    {

       // fearrect.transform.localPosition = new Vector3(((FearVal*(712 - 94)/100) + -712), fearrect.transform.localPosition.y, fearrect.transform.localPosition.z);
       // hoperect.transform.localPosition = new Vector3(((HopeVal * (712 - 94) / 100) + -712), hoperect.transform.localPosition.y, hoperect.transform.localPosition.z);

        //insert hersey checker based off percent of ai running around


    }

    // call this to change score
   public void ChangeScore(float addScore, byte Changetype)
    {
        if(Changetype == 1)
        {
            FaithVal += addScore;

        }

        if(Changetype == 2)
        {
            FearVal += addScore;

        }
        if(Changetype == 3)
        {
            HopeVal += addScore;

        }

    }






}
