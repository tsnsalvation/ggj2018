﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMenuController : MonoBehaviour
{
    public GameObject menuScreen;
    public Button resumeGame;
    public Button exitGame;

    public GameObject transitioningButton;

    public float lightningDelay;
    public float transitionTimer;
    public float maxTransitionTime;

    bool transitioning;

    public bool menuShowing;

    public Image lightningTransition;

    void Start()
    {
        menuScreen.SetActive(false);
        lightningTransition.gameObject.SetActive(false);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (menuShowing)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
        // if transitioning 
        if (!transitioning)
            return;
        if (transitionTimer > maxTransitionTime)
        {
            EndTransition();
        }
        else
        {
            transitionTimer += Time.deltaTime;
            //do something to the transitioning button
            if (transitionTimer > lightningDelay)
            {
                lightningTransition.fillAmount = (transitionTimer - lightningDelay) / (maxTransitionTime - lightningDelay);
            }
        }
    }
    public void ExitGame()
    {
        StartTransition(exitGame.gameObject);
        ResumeGame();
    }
    void PauseGame()
    {
        menuScreen.SetActive(true);
        menuShowing = true;
        Time.timeScale = 0f;
    }
    public void ResumeGame()
    {
        menuScreen.SetActive(false);
        menuShowing = false;
        Time.timeScale = 1f;
    }

    void EndTransition()
    {
        ToggleMenuButtons(true);
        if (transitioningButton == exitGame.gameObject)
        {
            Cursor.visible = true;
            SceneManager.LoadScene(0);
            return;
        }
        if (transitioningButton == resumeGame.gameObject)
        {
            ResumeGame();
            return;
        }
    }
    void StartTransition(GameObject transButton)
    {
        transitioningButton = transButton;
        transButton.GetComponent<Animator>().SetBool("Transition", true);
        lightningTransition.transform.position = transButton.transform.position;
        lightningTransition.fillAmount = 0;

        lightningTransition.gameObject.SetActive(true);
        ToggleMenuButtons(false);
        transitionTimer = 0f;
        transitioning = true;
    }
    public void ToggleMenuButtons(bool enabled)
    {
        exitGame.enabled = enabled;
        resumeGame.enabled = enabled;
    }
}
