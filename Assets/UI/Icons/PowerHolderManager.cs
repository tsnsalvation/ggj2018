﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PowerHolderManager : MonoBehaviour
{
    public MinonInfo infotoload;
    public Image npcPortrait;

    public bool active;
    public Text npcName;
    public Text flavaText;


    [Header("Power Buttons")]
    public Button Power1;
    public Button Power2;
    public Button Power3;
    public Button Power4;

    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        if (!active)
        {
            Power1.interactable = false;
            Power2.interactable = false;
            Power3.interactable = false;
            Power4.interactable = false;
        }
        else
        {
            Power1.interactable = true;
            Power2.interactable = true;
            Power3.interactable = true;
            Power4.interactable = true;
        }

    }
    public void ChangeSelect(Selectable pickedMinion)
    {

    }
}
