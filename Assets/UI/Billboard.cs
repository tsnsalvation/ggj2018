﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Billboard : MonoBehaviour
{
    Camera mainCamera;
    Text billboardText;

    void Awake()
    {
        billboardText = GetComponentInChildren<Text>();
    }

    void InitialiseCamera()
    {
        mainCamera = Camera.main;
    }
    void Update()
    {
        BoardUpdate();
    }
    public void BoardUpdate()
    {
        if (mainCamera == null)
        {
            InitialiseCamera();
            return;
        }
        transform.LookAt(transform.position + mainCamera.transform.rotation * Vector3.forward,
                         mainCamera.transform.rotation * Vector3.up);
    }
}