﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public Button newGameButton;
    public Button helpButton;
    public Button creditsButton;
    public Button exitButton;
    public Button backButton;

    public GameObject transitioningButton;
    public GameObject creditsScreen;
    public GameObject helpScreen;

    List<GameObject> showList;

    public float lightningDelay;
    public float transitionTimer;
    public float maxTransitionTime;

    bool transitioning;

    public Image lightningTransition;

    public AudioSource gearSound;
    public AudioSource zapSound;

    public AudioSource music;
    public bool fadeMusic; 
    public float fadeDelay;
    bool zapPlaying = false;

    // Use this for initialization
    void Start()
    {
        backButton.gameObject.SetActive(false);
        lightningTransition.gameObject.SetActive(false);
        showList = new List<GameObject>();
        fadeMusic = false;
        
    }
    void Update()
    {
        // if transitioning 
        if (!transitioning)
            return;
        if (transitionTimer > maxTransitionTime)
        {
            EndTransition();
        }
        else
        {
            transitionTimer += Time.deltaTime;
            //do something to the transitioning button
            if (transitionTimer > lightningDelay)
            {
                if (!zapPlaying)
                {
                    zapPlaying = true;
                    zapSound.Play();
                }
                lightningTransition.fillAmount = (transitionTimer - lightningDelay) / (maxTransitionTime - lightningDelay);
            }
        }
        if (fadeMusic)
        {
            music.volume -= (Time.deltaTime / fadeDelay);
        }
    }

    public void ShowCredits()
    {
        //toogle UI buttons and show Back button
        StartTransition(creditsButton.gameObject);
        newGameButton.gameObject.SetActive(false);
        helpButton.gameObject.SetActive(false);
        exitButton.gameObject.SetActive(false);
        showList.Add(backButton.gameObject);
    }

    public void ExitGame()
    {
        StartTransition(exitButton.gameObject);
    }
    public void StartGame()
    {
        StartTransition(newGameButton.gameObject);
        fadeMusic = true;
    }
    public void ShowHelp()
    {
        // toggle UI buttons and show back button
        StartTransition(helpButton.gameObject);
        newGameButton.gameObject.SetActive(false);
        creditsButton.gameObject.SetActive(false);
        exitButton.gameObject.SetActive(false);
        showList.Add(backButton.gameObject);

    }
    public void BackToMenu()
    {
        StartTransition(backButton.gameObject);
        showList.Add(newGameButton.gameObject);
        showList.Add(helpButton.gameObject);
        showList.Add(creditsButton.gameObject);
        showList.Add(exitButton.gameObject);
        //lightningTransition.fillOrigin = 1;
    }

    void EndTransition()
    {
        if (transitioningButton == exitButton.gameObject)
        {
            Application.Quit();
            return;

        }
        if (transitioningButton == newGameButton.gameObject)
        {
            SceneManager.LoadScene(1);
            return;
        }
        if (transitioningButton == creditsButton.gameObject)
        {
            creditsScreen.SetActive(true);
        }
        if (transitioningButton == helpButton.gameObject)
        {
            helpScreen.SetActive(true);
        }
        if (transitioningButton == backButton.gameObject)
        {
            creditsScreen.SetActive(false);
            helpScreen.SetActive(false);
        }
        ToggleMenuButtons(true);
        for (int i = 0; i < showList.Count; i++)
        {
            showList[i].SetActive(true);
        }
        showList.Clear();
        if (transitioningButton == helpButton.gameObject || transitioningButton == creditsButton.gameObject)
        {
            //lightningTransition.fillOrigin = 1;
            lightningTransition.transform.localScale = new Vector2(-1, 1);

        }
        else
        {
            //lightningTransition.fillOrigin = 0;
            lightningTransition.transform.localScale = new Vector2(1, 1);
        }
        gearSound.Stop();
        zapSound.Stop();
        //transitioningButton.GetComponent<Animator>().SetBool("Transition", false);
        transitioningButton.SetActive(false);
        lightningTransition.gameObject.SetActive(false);
    }
    void StartTransition(GameObject transButton)
    {
        transitioningButton = transButton;
        transButton.GetComponent<Animator>().SetBool("Transition", true);
        lightningTransition.transform.position = transButton.transform.position;
        lightningTransition.fillAmount = 0;

        lightningTransition.gameObject.SetActive(true);
        gearSound.Play();
        zapPlaying = false;
        ToggleMenuButtons(false);
        transitionTimer = 0f;
        transitioning = true;

    }
    public void ToggleMenuButtons(bool enabled)
    {
        newGameButton.enabled = enabled;
        helpButton.enabled = enabled;
        creditsButton.enabled = enabled;
        exitButton.enabled = enabled;
        backButton.enabled = enabled;
    }
}
