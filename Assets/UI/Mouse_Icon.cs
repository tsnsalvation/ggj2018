﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mouse_Icon : MonoBehaviour {
    public bool clicked;
    public bool powered;

    public Sprite Point;
    public Sprite Click;
    public Sprite Hold;

    public Animator powerCell;

    public Image cursorImage;
    
	// Use this for initialization
	void Start () {
        Cursor.visible = false;
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Input.mousePosition;

        if(Input.GetButton("Left Click") || Input.GetButton("Right Click"))
        {
            clicked = true;
        }
        else
        {
            clicked = false;
        }

        if(clicked)
        {
            cursorImage.sprite = Click;

        }
        else if (!clicked && !powered)
        {
            cursorImage.sprite = Point;

        }
        else
        {
            cursorImage.sprite = Hold;

        }

    }



  
}
