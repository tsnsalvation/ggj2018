﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseSelect : MonoBehaviour {

    public LayerMask selectableLayers;
    Camera playerCam;   
    public Selectable currentlySelected;
    StateController stateController;
    public GameObject selectedIndicator;

    public Mouse_Icon mouseIcon;

    public AudioSource getVertical;


    private void Awake()
    {
        if (GetComponent<Camera>() == null)
            throw new UnassignedReferenceException();
        else
            playerCam = Camera.main;
    }
    void Update()
    {
        if (Input.GetButtonDown("Left Click"))
        {
            RaycastHit hit;
            
            if (Physics.SphereCast(playerCam.ScreenPointToRay(Input.mousePosition), 1, out hit, Mathf.Infinity, selectableLayers))
            {
                if (hit.collider != null)
                {
                    if (hit.collider.tag == "Vertical")
                    {
                        if (!getVertical.isPlaying)
                            getVertical.Play();
                    }
                    if (hit.collider.GetComponent<Selectable>() != null)
                    {
                        currentlySelected = hit.collider.GetComponent<Selectable>() as Selectable;
                        selectedIndicator.transform.SetParent(currentlySelected.transform, false);
                        selectedIndicator.transform.localPosition = Vector3.zero;
                        selectedIndicator.transform.position += Vector3.up;
                        selectedIndicator.SetActive(true);
                        //interactMenu.UpdateButtons(currentlySelected);
                        //interactMenu.gameObject.SetActive(true);
                        GameManager.instance.selectedMinion = currentlySelected.GetComponentInParent<Minion>();
                        GameManager.instance.ShowPowerMenu();
                    }
                } 
            }
        }
        if (Input.GetButton("Right Click"))
        {
            if (currentlySelected != null)
            {
                currentlySelected.UnSelected();
				//currentlySelected.transform.parent = transform;
                currentlySelected = null;
            }
            selectedIndicator.SetActive(false);
            GameManager.instance.HidePowerMenu();
        }
        if (currentlySelected != null)
        {
            currentlySelected.Selected();
        }
        else
        {
            selectedIndicator.SetActive(false);
        }
    }
}
