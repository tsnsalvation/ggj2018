﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Attach to man camera 
public class PlayerControls : MonoBehaviour
{
    [Range(-100, 100)]
    public float alignment;
    [Range(0,100)]
    public float faith;
    [Range(-100,100)]
    public float following;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UsePower(float alignmentChange, float faithSpent)
    {
        alignment += alignmentChange;
        faith -= faithSpent;


    }
}
