﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * GAME USAGE:
 * When in play mode, use wasd keys to move the camera across the x and z planes. Use shift
 * to increase the speed at which the camera moves.
 * 
 * IMPLEMENTING:
 * Attached to CamAnchor, which the Player Camera is parented to. This allows a consistent 
 * distance to be kept, and adjusted, from the map and for the use of bounds to prevent player 
 * from scrolling outside of the game area
 */
public class CameraControls : MonoBehaviour {

    [Range(0, 100)]
    public float regularMoveSpeed = 40;
    [Range(0, 100)]
    public float fastMoveSpeed = 80;

    [Range(38,100)]
    float zoom = 20;
    float zoomMin = 0;
    float zoomMax = 35;
    public float cameraZoomSpeed = 18f;

    float lerpTime = 0.5f;
    float currentLerpTime;
    bool lerpingZoom;

    Vector3 startPos;
    Vector3 endPos;

    public Vector3 minBounds;
    public Vector3 maxBounds;

    public void FixedUpdate()
    {
        float moveSpeed;
        if (!Input.GetButton("Shift"))
        {
            moveSpeed = regularMoveSpeed;
        }
        else
        {
            moveSpeed = fastMoveSpeed;
        }
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float forwardInput = Input.GetAxisRaw("Forward");
        if ((horizontalInput != 0) || (forwardInput != 0))
        {
            Vector3 newPosition = new Vector3(transform.position.x + horizontalInput, transform.position.y, transform.position.z + forwardInput);
            if ((newPosition.x < minBounds.x) || (newPosition.x > maxBounds.x))
            {
                if (newPosition.x < minBounds.x)
                {
                    if ((newPosition.z > minBounds.z) && (newPosition.z < maxBounds.z))
                    {
                        newPosition = new Vector3(newPosition.x - horizontalInput, newPosition.y, newPosition.z);
                    }
                    else
                    {
                        if (newPosition.z < minBounds.z)
                        {
                            if ((newPosition.x > minBounds.x) && (newPosition.x < maxBounds.x))
                            {
                                newPosition = new Vector3(newPosition.x, newPosition.y, newPosition.z - forwardInput);
                            }
                        }
                        else
                        {
                            if (newPosition.z > maxBounds.z)
                            {
                                if ((newPosition.x > minBounds.x) && (newPosition.x < maxBounds.x))
                                {
                                    newPosition = new Vector3(newPosition.x, newPosition.y, newPosition.z - forwardInput);
                                }
                            }
                        }
                    }
                }
                if (newPosition.x > maxBounds.x)
                {
                    if ((newPosition.z > minBounds.z) && (newPosition.z < maxBounds.z))
                    {
                        newPosition = new Vector3(newPosition.x - horizontalInput, newPosition.y, newPosition.z);
                    }
                    else
                    {
                        if (newPosition.z < minBounds.z)
                        {
                            if ((newPosition.x > minBounds.x) && (newPosition.x < maxBounds.x))
                            {
                                newPosition = new Vector3(newPosition.x, newPosition.y, newPosition.z - forwardInput);
                            }
                        }
                        else
                        {
                            if (newPosition.z > maxBounds.z)
                            {
                                if ((newPosition.x > minBounds.x) && (newPosition.x < maxBounds.x))
                                {
                                    newPosition = new Vector3(newPosition.x, newPosition.y, newPosition.z - forwardInput);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (newPosition.z < minBounds.z)
                {
                    if ((newPosition.x > minBounds.x) && (newPosition.x < maxBounds.x))
                    {
                        newPosition = new Vector3(newPosition.x, newPosition.y, newPosition.z - forwardInput);
                    }
                    else
                    {
                        if (newPosition.x < minBounds.x)
                        {
                            if ((newPosition.z > minBounds.z) && (newPosition.z < maxBounds.z))
                            {
                                newPosition = new Vector3(newPosition.x - horizontalInput, newPosition.y, newPosition.z);
                            }
                        }
                        else
                        {
                            if (newPosition.x > maxBounds.x)
                            {
                                if ((newPosition.z > minBounds.z) && (newPosition.z < maxBounds.z))
                                {
                                    newPosition = new Vector3(newPosition.x - horizontalInput, newPosition.y, newPosition.z);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (newPosition.z > maxBounds.z)
                    {
                        if ((newPosition.x > minBounds.x) && (newPosition.x < maxBounds.x))
                        {
                            newPosition = new Vector3(newPosition.x, newPosition.y, newPosition.z - forwardInput);
                        }
                        else
                        {
                            if (newPosition.x < minBounds.x)
                            {
                                if ((newPosition.z > minBounds.z) && (newPosition.z < maxBounds.z))
                                {
                                    newPosition = new Vector3(newPosition.x - horizontalInput, newPosition.y, newPosition.z);
                                }
                            }
                            else
                            {
                                if (newPosition.x > maxBounds.x)
                                {
                                    if ((newPosition.z > minBounds.z) && (newPosition.z < maxBounds.z))
                                    {
                                        newPosition = new Vector3(newPosition.x - horizontalInput, newPosition.y, newPosition.z);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if ((newPosition.x > minBounds.x) &&
                (newPosition.x < maxBounds.x) &&
                (newPosition.z > minBounds.z) &&
                (newPosition.z < maxBounds.z))
            {
                transform.position = Vector3.Lerp(transform.position, newPosition, moveSpeed / 100);
            }
        }
        if (!lerpingZoom)
        {

            var scrollZoom = -Input.GetAxis("Mouse ScrollWheel") * cameraZoomSpeed;
            if (scrollZoom > 0f)
            {
                zoom += scrollZoom;
                if (zoom > zoomMax)
                {
                    zoom = zoomMax;
                }
                startPos = transform.position;
                endPos = new Vector3(transform.position.x, zoom, transform.position.z);
                currentLerpTime = 0f;
                lerpingZoom = true;
            }
            else if (scrollZoom < 0f)
            {
                zoom += scrollZoom;
                if (zoom < zoomMin)
                {
                    zoom = zoomMin;
                }
                startPos = transform.position;
                endPos = new Vector3(transform.position.x, zoom, transform.position.z);
                currentLerpTime = 0f;
                lerpingZoom = true;
            }  
        }
        if (lerpingZoom)
        {
            LerpCamera();
        }
        
    }
    void LerpCamera()
    {
        currentLerpTime += Time.deltaTime;
        if (currentLerpTime > lerpTime)
        {
            currentLerpTime = lerpTime;
            lerpingZoom = false;
        }

        //lerp!
        float perc = currentLerpTime / lerpTime;
        //transform.position = Vector3.Lerp(startPos, endPos, perc );
		Camera.main.orthographicSize = endPos.y;
    }
}
