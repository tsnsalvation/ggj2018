﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalInteractMenu : MonoBehaviour {

    public GameObject inspireButton;
    public GameObject smiteButton;
    public Selectable currentlySelected;

    private void Awake()
    {
        if ((GameObject.FindObjectOfType<GlobalInteractMenu>() != null) && (GameObject.FindObjectOfType<GlobalInteractMenu>() != this))
            Destroy(this.gameObject);
    }

    private void Update()
    {
        if (currentlySelected != null)
            UpdateButtons(currentlySelected);
    }
    public void UpdateButtons(Selectable selected)
    {
        currentlySelected = selected;
        if (currentlySelected.canInspire)
            inspireButton.SetActive(true);
        else
            inspireButton.SetActive(false);
        if (currentlySelected.canSmite)
            smiteButton.SetActive(true);
        else
            smiteButton.SetActive(false);
    }
}
