﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePower : MonoBehaviour
{
    public AudioSource abilitySound;
    public ParticleSystem particleEffect;

    public bool hasDelay;
    public float delayTime;

    public float abilityRange;
    public float abilityStrength;



    public void ActivateAbility()
    {
        if (hasDelay)
        {
            // trigger delay
        }
        else
        {
            TriggerAbility();
        }
    }

    public void TriggerAbility()
    {

    }
}
