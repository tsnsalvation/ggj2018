﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public float faith;
    public float alignment;
    public float belief;
    public float decayRate;
    
    public static GameManager instance;
	public AudioSource powerSound;

    public PopulationControl population;
    public CharacterProfiles npcProfiles;

    public float earnFaithTickTime;
    float currentTickTime;

    public Minion selectedMinion;
    public GameObject powerMenu;
    public PowerHolderManager phm;

    public MouseFollow mouseFollow;

    public Power inspireFear;
    public Power inspireHope;
    public Power manifestFear;
    public Power manifestHope;
    public Power smiteFear;
    public Power smiteHope;
	public Power inquisition;


    public AudioSource menuGears;

    // Use this for initialization
    private void Awake()
    {
        if (instance == null)
            instance = this;

    }

	public void winState ()
	{

	}

	public void loseState ()
	{

	}

    void Start()
    {
        if (instance == null)
            instance = this;
        faith = 5;
        mouseFollow.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        TickFaith();
    }

    public void TickFaith()
    {
        currentTickTime += Time.deltaTime;
        if (currentTickTime > earnFaithTickTime)
        {
            //Tick
            belief = population.getPositiveBelief();

			if (Mathf.Approximately (belief, 0f)) {
				loseState ();
			}

            faith += (belief + 100) / 200f * 10; //could use an animation curve to controll the flow so that you still get faith when shit is bad
            currentTickTime = 0;
        }
    }

    public void PowerUsed(float alignmentChange, float faithSpent)
    {
        alignment += alignmentChange;
        faith -= faithSpent;
    }

    public void ShowPowerMenu()
    {
        menuGears.Play(); 
        var minionDeets = selectedMinion.GetComponent<Minion_Apperance>();
        var npcFullName = "";
        if (minionDeets.male)
        {
            npcFullName += npcProfiles.maleFirstNames[minionDeets.firstName] + " ";
            npcFullName += npcProfiles.maleFirstNames[minionDeets.middleName] + " ";
            npcFullName += npcProfiles.maleFirstNames[minionDeets.lastName];
        }
        else
        {
            npcFullName += npcProfiles.femaleFirstNames[minionDeets.firstName] + " ";
            npcFullName += npcProfiles.femaleMiddleNames[minionDeets.middleName] + " ";
            npcFullName += npcProfiles.femaleLastNames[minionDeets.lastName];
        }
        phm.npcName.text = npcFullName;
        
        if (selectedMinion.type == Minion.MinionType.Cleric)
        {
            phm.flavaText.text = npcProfiles.clericBackStory[minionDeets.backStory];
            phm.npcPortrait.sprite = npcProfiles.npcImages[1];
        }
        else if (selectedMinion.type == Minion.MinionType.Lunatic)
        {
            phm.flavaText.text = npcProfiles.lunaticBackStory[minionDeets.backStory];
            phm.npcPortrait.sprite = npcProfiles.npcImages[0];
        }
        else if (selectedMinion.type == Minion.MinionType.Scholar)
        {
            phm.flavaText.text = npcProfiles.scholarBackStory[minionDeets.backStory];
            phm.npcPortrait.sprite = npcProfiles.npcImages[2];
        }
        else //commoner or sinner
        {
            phm.flavaText.text = npcProfiles.commonorBackStory[minionDeets.backStory];
            phm.npcPortrait.sprite = npcProfiles.npcImages[3];
        }
        

        powerMenu.GetComponent<Animator>().SetBool("Revealing", true);
        powerMenu.GetComponent<Animator>().SetBool("Hidding", false);
        powerMenu.GetComponentInChildren<PowerHolderManager>().active = true;
    }
    public void HidePowerMenu()
    {
        menuGears.Play();
        powerMenu.GetComponent<Animator>().SetBool("Hidding", true);
        powerMenu.GetComponent<Animator>().SetBool("Revealing", false);
        powerMenu.GetComponentInChildren<PowerHolderManager>().active = false;
    }

    public void InspireFearClick()
    {
        if (selectedMinion != null)
        {
            inspireFear.activatePower(selectedMinion);
        }
    }
    public void InspireHopeClick()
    {
        if (selectedMinion != null)
        {
			inspireHope.activatePower(selectedMinion);
        }
    }
    public void ManifestFearClick()
    {
        //if (selectedMinion != null)
        //{
			manifestFear.activatePower(selectedMinion);
        //}
    }
    public void ManifestHopeClick()
    {
       // if (selectedMinion != null)
       // {
			manifestHope.activatePower(selectedMinion);
        //}
    }
    public void SmiteFearClick()
    {
        if (selectedMinion != null)
        {
			smiteFear.activatePower(selectedMinion);
        }
    }
    public void SmiteHopeClick()
    {
        if (selectedMinion != null)
        {
			smiteHope.activatePower(selectedMinion);
        }
    }
	public void InquisitionClick ()
	{
		inquisition.activateInquisition ();
	}
}
