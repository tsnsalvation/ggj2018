﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Power")]
public class Power : ScriptableObject {
	
	public enum PowerType {Manifest, Inquisitor, Prophet, Smite, Infrastructure, Inquisition};
	public PowerType type;
	public float faithCost;
	public float alignmentDelta;
	public float decayRateDelta;
	public float force;
	public float radius;
	public float animDelay;
	//public GameObject animationObject;
	public AudioClip soundEffect;

	public void activateInquisition ()
	{
		if (soundEffect) {
			GameManager.instance.powerSound.clip = soundEffect;
			GameManager.instance.powerSound.Play ();
			//AudioSource.PlayClipAtPoint (soundEffect, minionTarget.transform.position);
		}

		PopulationControl.instance.StartCoroutine (
			PopulationControl.instance.inquisition (force));
	}

	public void retrieveAnimator ()
	{
		//animationObject.GetComponent<PowerAnimator> ().hideAnimator ();
	}

	public void activatePower (Minion minionTarget)
	{
		if (GameManager.instance.faith > faithCost) {
			GameManager.instance.faith -= faithCost;
			GameManager.instance.alignment += alignmentDelta;
			//animationObject.GetComponent<PowerAnimator>().target = minionTarget.transform;
			//animationObject.GetComponent<Animator>().SetTrigger("activate");

			if (soundEffect) {
				GameManager.instance.powerSound.clip = soundEffect;
				GameManager.instance.powerSound.Play ();
				//AudioSource.PlayClipAtPoint (soundEffect, minionTarget.transform.position);
			}

			if (type == PowerType.Inquisitor) {
				inquisitor (minionTarget);
			} else if (type == PowerType.Prophet) {
				prophet (minionTarget);
			} else if (type == PowerType.Smite) {
				smitePower (minionTarget);
			}

		}
	}

	public void activatePower () //Ground targeted
	{
		if (soundEffect) {
			GameManager.instance.powerSound.clip = soundEffect;
			GameManager.instance.powerSound.Play ();
			//AudioSource.PlayClipAtPoint (soundEffect, minionTarget.transform.position);
		}


		if (GameManager.instance.faith > faithCost) {
			RaycastHit hitInfo;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hitInfo) && GameManager.instance.faith > faithCost) {
				if (GameManager.instance.faith > faithCost) {
					manifestPower (hitInfo.point);
				}
			}
		}
	}

	void manifestPower (Vector3 locationTarget)
	{
		if (soundEffect) {
			GameManager.instance.powerSound.clip = soundEffect;
			GameManager.instance.powerSound.Play ();
			//AudioSource.PlayClipAtPoint (soundEffect, minionTarget.transform.position);
		}

		GameManager.instance.faith -= faithCost;
		GameManager.instance.alignment += alignmentDelta;

		RaycastHit [] hitInfo = Physics.SphereCastAll (locationTarget += Vector3.up * 5f, radius, Vector3.down * 10f);
		foreach (RaycastHit hit in hitInfo) {
			Minion minion = hit.collider.gameObject.GetComponent<Minion> ();
			if (minion) {
				float distanceToMinion = Vector3.Distance (minion.transform.position, hit.point);
				float falloffPercent = 1 / (distanceToMinion + 1);
				minion.belief.updateBelief (force * falloffPercent, true);
			}
		}
	}

	void inquisitor (Minion minionTarget)
	{
		PopulationControl.instance.inquisitorInspire (minionTarget);
	}

	void prophet (Minion minionTarget)
	{
		PopulationControl.instance.prophetInspire (minionTarget);
	}

	void smitePower (Minion minionTarget)
	{
		PopulationControl.instance.smiteMinion (minionTarget, animDelay, this);
	}
}
