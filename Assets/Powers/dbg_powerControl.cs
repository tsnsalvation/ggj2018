﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dbg_powerControl : MonoBehaviour {

	public Power powerLeftClick, powerRightClick;

	void Update ()
	{
		if (Input.GetMouseButtonDown (0)) {
			powerLeftClick.activatePower ();
		} else if (Input.GetMouseButtonDown (1)) {
			powerRightClick.activatePower ();
		}
	}

}
