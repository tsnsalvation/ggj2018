﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopulationControl : MonoBehaviour {

	public Transform spawnPlane; //Spawnplane assumes a regular Unity plane sized over the gamearea
	public static PopulationControl instance;
	public Minion sinner;

	public GameObject prophetIndicator, inquisitorIndicator;

	public List <Minion> commoners = new List <Minion>();
	public List <Minion> scholars = new List <Minion>();
	public List <Minion> lunatics = new List <Minion>();
	public List <Minion> clerics = new List <Minion>();
	public List <Minion> inquisitors = new List <Minion>();
	public List <Minion> prophets = new List <Minion> ();

	[Range(0,50)]
	public int targetScholar = 10;
	[Range(0,50)]
	public int targetLunatic = 10;
	[Range(0,50)]
	public int targetCleric = 10;

	public int targetPopulation = 750;
	public GameObject obj_sinner, obj_commoner, obj_scholar, obj_lunatic, obj_cleric;

	public bool debugView = false;
	private Material dbgCommon, dbgScholar, dbgLunatic, dbgCleric, dbgSinner;

	public void smiteMinion (Minion target, float delay, Power power)
	{
		StartCoroutine (smiteDelay (target, delay, power));
	}

	IEnumerator smiteDelay (Minion target, float delay, Power power)
	{
		yield return new WaitForSeconds (delay);

		if (target.type == Minion.MinionType.Sinner) {
			GameManager.instance.winState ();
		}

		removeMinion (target);
		GameManager.instance.selectedMinion = null;
		Camera.main.GetComponent<MouseSelect> ().currentlySelected = null;
		Camera.main.GetComponent<MouseSelect> ().selectedIndicator.transform.parent = null;
		Camera.main.GetComponent<MouseSelect> ().selectedIndicator.SetActive (false);
		//power.animationObject.GetComponent<PowerAnimator> ().target = null;
		Destroy (target.gameObject);
	}

	void removeMinion (Minion target)
	{
		if (target.type == Minion.MinionType.Commoner) {
			commoners.Remove (target);		
		} else if (target.type == Minion.MinionType.Cleric) {
			clerics.Remove (target);
		} else if (target.type == Minion.MinionType.Lunatic) {
			lunatics.Remove (target);
		} else if (target.type == Minion.MinionType.Scholar) {
			scholars.Remove (target);
		} else if (target.type == Minion.MinionType.Inquisitor) {
			inquisitors.Remove (target);
		} else if (target.type == Minion.MinionType.Prophet) {
			prophets.Remove (target);
		} else if (target.type == Minion.MinionType.Sinner) {
			Debug.Log ("You removed the sinner");
		}
	}

	public void inquisitorInspire (Minion replaced)
	{
		if (replaced.type == Minion.MinionType.Sinner) {
			return;
		}

		foreach (Transform child in replaced.spriteHolder) {
			//child.GetComponent<Renderer> ().material.color = Color.red;
		}
		replaced.gameObject.AddComponent<Minion_Inquisitor> ();
		GameObject newIndicator = Instantiate (inquisitorIndicator, replaced.classIndicator.position, replaced.classIndicator.rotation, replaced.classIndicator.transform.parent);
		Destroy (replaced.classIndicator.gameObject);
		replaced.classIndicator = newIndicator.transform;
	}

	public void prophetInspire (Minion replaced)
	{
		if (replaced.type == Minion.MinionType.Sinner) {
			return;
		}

		foreach (Transform child in replaced.spriteHolder) {
			//child.GetComponent<Renderer> ().material.color = Color.Lerp (Color.white, Color.yellow, 0.5f);
		}
		GameObject newIndicator = Instantiate (prophetIndicator, replaced.classIndicator.position, replaced.classIndicator.rotation, replaced.classIndicator.transform.parent);
		Destroy (replaced.classIndicator.gameObject);
		replaced.classIndicator = newIndicator.transform;
	}

	public IEnumerator inquisition (float duration)
	{
		toggleBeliefIndicator (true);
		yield return new WaitForSeconds (duration);
		toggleBeliefIndicator (false);
	}

	void toggleBeliefIndicator (bool toggle)
	{
		sinner.GetComponentInChildren<BeliefIndicator> ().visible = toggle;
		foreach (Minion minion in commoners) {
			minion.GetComponentInChildren<BeliefIndicator> ().visible = toggle;
		}
		foreach (Minion minion in clerics) {
			minion.GetComponentInChildren<BeliefIndicator> ().visible = toggle;
		}
		foreach (Minion minion in lunatics) {
			minion.GetComponentInChildren<BeliefIndicator> ().visible = toggle;
		}
		foreach (Minion minion in scholars) {
			minion.GetComponentInChildren<BeliefIndicator> ().visible = toggle;
		}
		foreach (Minion minion in prophets) {
			minion.GetComponentInChildren<BeliefIndicator> ().visible = toggle;
		}
	}

	void Awake ()
	{
		if (instance == null) {
			instance = this;
		}
		setMaterial ();
		sinner = initialSpawn(obj_sinner, Minion.MinionType.Sinner);
		for (int i = 0; i < targetScholar; i++) {
			scholars.Add(initialSpawn (obj_scholar, Minion.MinionType.Scholar));
		}
		for (int i = 0; i < targetLunatic; i++) {
			lunatics.Add (initialSpawn (obj_lunatic, Minion.MinionType.Lunatic));
		}
		for (int i = 0; i < targetCleric; i++) {
			clerics.Add (initialSpawn (obj_cleric, Minion.MinionType.Cleric));
		}
		for (int i = currentPopulation(); i < targetPopulation; i++) {
			commoners.Add (initialSpawn (obj_commoner, Minion.MinionType.Commoner));
		}
	}

	void Start ()
	{
		if (debugView) {
			applyMaterial (commoners, dbgCommon);
			applyMaterial (scholars, dbgScholar);
			applyMaterial (lunatics, dbgLunatic);
			applyMaterial (clerics, dbgCleric);
			sinner.dbgIndicator.enabled = true;
			sinner.dbgIndicator.material = dbgSinner;
			sinner.type = Minion.MinionType.Sinner;
		}
	}

	public float getPositiveBelief ()
	{
		float positiveBelief = 0f;
		positiveBelief += netPositive (scholars);
		positiveBelief	+= netPositive (clerics);
		positiveBelief += netPositive (lunatics);
		positiveBelief += netPositive (commoners);
		positiveBelief = positiveBelief / currentPopulation ();
		return positiveBelief;
	}

	float netPositive (List <Minion> community)
	{
		float net = 0f;
		foreach (Minion minion in community) {
			if (minion.belief.belief > 0f) {
				net += (minion.belief.belief);
			}
		}
		return net;
	}

	void setMaterial ()
	{
		dbgCommon = new Material (Shader.Find ("Standard"));
		dbgScholar = new Material (dbgCommon);
		dbgScholar.color = Color.blue;
		dbgLunatic = new Material (dbgCommon);
		dbgLunatic.color = Color.green;
		dbgCleric = new Material (dbgCommon);
		dbgCleric.color = Color.yellow;
		dbgSinner = new Material (dbgCommon);
		dbgSinner.color = Color.red;
	}

	void applyMaterial (List <Minion> minions, Material mat)
	{
		foreach (Minion minion in minions) {
			//Renderer rend = minion.GetComponent<Renderer> ();
			minion.dbgIndicator.enabled = true;
			minion.dbgIndicator.material = mat;
		}
	}

	int currentPopulation ()
	{
		int current = commoners.Count + scholars.Count + lunatics.Count + clerics.Count;
		return current;
	}

	Minion initialSpawn (GameObject prefab, Minion.MinionType type)
	{
		Vector3 spawnPoint = getSpawnPoint ();
		GameObject obj = Instantiate (prefab, spawnPoint, transform.rotation, transform);
		Minion minion = obj.GetComponent<Minion> ();
		minion.type = type;
		return minion;
	}

	Vector3 getSpawnPoint ()
	{
		Vector3 spawnPoint;
		Vector3 midPoint = spawnPlane.position;
		float randomX = 5f * Random.Range (-spawnPlane.localScale.x, spawnPlane.localScale.x);
		float randomZ = 5f * Random.Range (-spawnPlane.localScale.z, spawnPlane.localScale.z);
		midPoint += (Vector3.right * randomX);
		midPoint += (Vector3.forward * randomZ);

		RaycastHit hitInfo;
		Physics.Raycast (midPoint, Vector3.down, out hitInfo, Mathf.Infinity);
		if (hitInfo.collider.CompareTag ("Floor")) {
			spawnPoint = hitInfo.point + Vector3.up * 0.75f; //Assuming standard sphere
		} else {
			spawnPoint = getSpawnPoint ();
		}
		return spawnPoint;
	}
}
