﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "CharacterProfiles")]
public class CharacterProfiles : ScriptableObject
{
    public List<string> maleFirstNames = new List<string>();
    public List<string> maleMiddleNames = new List<string>();
    public List<string> maleLastNames = new List<string>();

    public List<string> femaleFirstNames = new List<string>();
    public List<string> femaleMiddleNames = new List<string>();
    public List<string> femaleLastNames = new List<string>();

    public List<string> clericBackStory = new List<string>();
    public List<string> lunaticBackStory = new List<string>();
    public List<string> scholarBackStory = new List<string>();
    public List<string> commonorBackStory = new List<string>();

    public List<Sprite> npcImages = new List<Sprite>();
}
