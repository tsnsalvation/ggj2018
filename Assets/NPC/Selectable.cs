﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selectable : MonoBehaviour {

    public bool canInspire;
    public bool canSmite;

    public void Selected()
    {

    }
    
    public void UnSelected()
    {

    }
}
