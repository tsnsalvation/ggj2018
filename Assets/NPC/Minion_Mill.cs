﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Minion_Mill : MonoBehaviour {

	public enum States {Mill, Talk, Group};
	[HideInInspector]
	public States state = States.Mill;

	float bounceChance = 40f;
	float talkChance = 20f;
	float groupUpChance = 40f;

	float moveSpeed = 5f;

	public bool active = true;
	float talkTime = 2f; 
	float groupTime = 2f;

	[HideInInspector]
	public Rigidbody rb;

	public bool debugView = false;
	Material dbgWalk, dbgTalk, dbgGroup;

	void Start ()
	{
		rb = GetComponent <Rigidbody> ();
		rb.velocity = getRandomFlatDirection ();
		applySettings (GetComponent<Minion> ().settings.mill);
	}

	public void applySettings (Minion_Settings.MillSetting settings)
	{
		bounceChance = settings.bounceChance;
		talkChance = settings.talkChance;
		groupUpChance = settings.groupUpChance;
		moveSpeed = settings.moveSpeed;
		talkTime = settings.talkTime;
		groupTime = settings.groupTime;
		dbgWalk = settings.dbgWalk;
		dbgTalk = settings.dbgTalk;
		dbgGroup = settings.dbgGroup;
	}

	public void updateFromController ()
	{
		rb.velocity = rb.velocity.normalized * moveSpeed;
	}
		
	public Vector3 getRandomFlatDirection ()
	{
		Vector3 direction = Vector3.zero;
		direction.x = Random.Range (-1f, 1f);
		direction.z = Random.Range (-1f, 1f);
		direction = direction.normalized * moveSpeed;
		return direction;
	}

	/*void OnCollisionEnter (Collision col)
	{
		Minion_Mill otherMinion = col.gameObject.GetComponent<Minion_Mill> ();
		if (otherMinion && active) {
			interactionChoice (otherMinion, col);
		}
	}*/

	public void interactionChoice (Minion_Mill otherMinion, Collision col)
	{
		float random = Random.Range (0f, 100f);
		if (random < bounceChance) {
			bounce (otherMinion, col);
		} else if (random < talkChance + bounceChance) {
			talk (otherMinion, col);
		} else if (random < groupUpChance + talkChance + bounceChance) {
			groupUp (otherMinion, col);
		}
	}

	void bounce (Minion_Mill otherMinion, Collision col)
	{
		active = false;
		rb.velocity = flattenNormalDirection (col.contacts [0].normal);
		StartCoroutine (do_bounce ());
		otherMinion.active = false;
		otherMinion.rb.velocity = flattenNormalDirection (col.contacts [0].normal) * moveSpeed;
		otherMinion.StartCoroutine (otherMinion.do_bounce ());
	}

	IEnumerator do_bounce ()
	{
		yield return new WaitForSeconds (0.5f);
		active = true;
	}

	void talk (Minion_Mill otherMinion, Collision col)
	{
		active = false;
		state = States.Talk;
		if (debugView) {
			GetComponent<Minion> ().dbgIndicator.material = dbgTalk;
		}
		rb.isKinematic = true;
		StartCoroutine (stopConversation ());
		otherMinion.active = false;
		otherMinion.state = States.Talk;
		if (debugView) {
			otherMinion.GetComponent<Minion> ().dbgIndicator.material = dbgTalk;
		}
		otherMinion.rb.isKinematic = true;
		otherMinion.StartCoroutine (otherMinion.stopConversation ());
	}

	IEnumerator stopConversation ()
	{
		yield return new WaitForSeconds (talkTime);
		active = true;
		rb.isKinematic = false;
		rb.velocity = getRandomFlatDirection ();
		if (debugView) {
			GetComponent<Minion> ().dbgIndicator.material = dbgWalk;
		}
		state = States.Talk;
	}

	void groupUp (Minion_Mill otherMinion, Collision col)
	{
		active = false;
		state = States.Group;
		if (debugView) {
			GetComponent<Minion> ().dbgIndicator.material = dbgGroup;
		}
		StartCoroutine (stopGroup ());

		otherMinion.active = false;
		otherMinion.state = States.Group;
		if (debugView) {
			otherMinion.GetComponent<Minion> ().dbgIndicator.material = dbgGroup;
		}
		otherMinion.StartCoroutine (otherMinion.stopGroup ());
	}

	IEnumerator stopGroup ()
	{
		yield return new WaitForSeconds (groupTime);
		active = true;
		rb.velocity = getRandomFlatDirection ();
		if (debugView) {
			GetComponent<Minion> ().dbgIndicator.material = dbgWalk;
		}
		state = States.Mill;
	}

	Vector3 flattenNormalDirection (Vector3 input)
	{
		input.y = 0f;
		Vector3 output = input.normalized;
		return output;
	}
}