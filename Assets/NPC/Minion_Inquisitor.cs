﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minion_Inquisitor : MonoBehaviour {

	public float revealRadius = 5f;

	void Awake ()
	{
		SphereCollider col = gameObject.AddComponent<SphereCollider> ();
		col.radius = revealRadius;
		col.isTrigger = true;
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.GetComponent<Minion> ()) {
			other.GetComponentInChildren<BeliefIndicator> ().visible = true;
		}
	}

	void OnTriggerExit (Collider other)
	{
		if (other.GetComponent<Minion> ()) {
			other.GetComponentInChildren<BeliefIndicator> ().visible = false;
		}
	}

}
