﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Minion_Belief : MonoBehaviour {

	[Range(-1f, 1f)]
	public float belief = 0f;
	float beliefVariance = 0.5f;
	float passingModifier = 0.5f;
	float talkingModifier = 2f;

	float force = 1f;
	float range = 1f;
	float delay = 1f;

	bool decay = true;
	float decayRate = 10f;
	public bool canConvert = true;

	public bool debugView = false;
	Material dbgNeutral, dbgHeretic, dbgPious;

	void Start ()
	{
		applySettings (GetComponent<Minion_Settings> ().belief);
		float startBelief = belief;
		updateBelief (0f, true);
	}

	public void applySettings (Minion_Settings.BeliefSetting settings)
	{
		beliefVariance = settings.beliefVariance;
		passingModifier = settings.passingModifier;
		talkingModifier = settings.talkingModifier;
		force = settings.force;
		range = settings.range;
		delay = settings.delay;
		decay = settings.decay;
		decayRate = settings.decayRate;
		dbgNeutral = settings.dbgNeutral;
		dbgHeretic = settings.dbgHeretic;
		dbgPious = settings.dbgPious;
	}

	void Update ()
	{
		if (decay && belief > 0f) {
			belief -= Time.deltaTime * GameManager.instance.decayRate * decayRate;
		} else if (decay && belief < 0f) {
			belief += Time.deltaTime * decayRate;
		}
	}

	void OnCollisionEnter (Collision col)
	{
		Minion_Belief otherMinion = col.gameObject.GetComponent<Minion_Belief> ();
		if (otherMinion && canConvert && otherMinion.canConvert) {
			conversionCheck (otherMinion);
		}
	}

	IEnumerator resetConvert ()
	{
		yield return new WaitForSeconds (delay);
		canConvert = true;
	}

	public void conversionCheck (Minion_Belief otherMinion)
	{
		canConvert = false;
		otherMinion.canConvert = false;


		float myForce = Random.Range (beliefVariance * (force * belief), force * belief);
		float otherForce = Random.Range 
			(otherMinion.beliefVariance * (otherMinion.force * otherMinion.belief), 
				otherMinion.force * otherMinion.belief);
		float delta = (myForce + otherForce) * passingModifier;
		if (GetComponent<Minion_Mill> ().state == Minion_Mill.States.Talk) {
			delta = (myForce + otherForce) * talkingModifier; //Higher belief change when talking
		}

		//Debug.Log ("Conversion attempt by " + transform.name + " and " + otherMinion.name + ": " + delta);

		if (decay) {
			updateBelief (delta, false);
		}
		if (otherMinion.decay) {
			otherMinion.updateBelief (delta, false);
		}

		updateBelief (delta, false);
		otherMinion.updateBelief (delta, false);

		areaEffect (delta * 0.5f, otherMinion.gameObject);
		otherMinion.areaEffect (delta * 0.5f, gameObject);

		StartCoroutine (resetConvert ());
		otherMinion.StartCoroutine (otherMinion.resetConvert ());
	}

	void areaEffect (float delta, GameObject otherMinion)
	{
		//Weird below-from-above done to make sure it hits
		float radius = GetComponent<SphereCollider>().radius + range;
		RaycastHit[] hitInfo = Physics.SphereCastAll
			(transform.position + Vector3.down * 5f, radius, Vector3.up * 10f);
		foreach (RaycastHit hit in hitInfo) {
			Minion_Belief minion = hit.collider.GetComponent<Minion_Belief> ();
			if (hit.collider.gameObject != gameObject && minion && minion.decay) {
				float distance = Vector3.Distance (transform.position, hit.transform.position);
				minion.updateBelief (delta / distance, false);
			}
		}
	}

	public void updateBelief (float delta, bool forceUpdate)
	{
		if (canConvert || forceUpdate) {
			belief += delta;
		}

		if (belief > 1f) {
			belief = 1f;
		} else if (belief < -1f) {
			belief = -1f;
		}

		if (belief > 0f) {
			if (debugView) {
				GetComponent<Minion> ().dbgIndicator.material.Lerp (dbgNeutral, dbgPious, belief);
			}
		} else {
			if (debugView) {
				GetComponent<Minion> ().dbgIndicator.material.Lerp (dbgNeutral, dbgHeretic, -belief);
			}
		}
	}

}
