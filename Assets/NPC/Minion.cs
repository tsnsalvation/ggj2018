﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script requires that minions have the "Minions" tag 
//Script requires that floors have the "Floor" tag
//Works with or without StateController

//TO INITIALISE:
//Create a sphere and drop this script onto it
//It will automatically build the sphere collider, rb, and everthing else needed
//Then drop graphical elements, configurations, etc into a child
//Child will require a billboarding script



[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]

[RequireComponent(typeof(Minion_Settings))]
[RequireComponent(typeof(Minion_Mill))]
[RequireComponent(typeof(Minion_Belief))]
public class Minion : MonoBehaviour {

	public enum MinionType {Commoner, Scholar, Lunatic, Cleric, Sinner, Inquisitor, Prophet};
	public MinionType type;

	public Transform classIndicator;
	public Transform spriteHolder;

	public Minion_Settings settings;
	public Minion_Mill mill;
	public Minion_Belief belief;
	public StateController stateController;
	public Renderer dbgIndicator;

	void Awake ()
	{
		mill = GetComponent<Minion_Mill> ();
		belief = GetComponent<Minion_Belief> ();
		settings.setMaterial ();
		if (mill.debugView || belief.debugView) {
			dbgIndicator.enabled = true; //This is the dbg sphere renderer
		} else {
			dbgIndicator.enabled = false;
		}
	}

	void Update ()
	{
		StateController stateControl = GetComponent<StateController> ();
		if (stateControl && stateControl.mill) {
			mill.updateFromController ();
		} else if (!stateControl) {
			mill.updateFromController ();
		}

		if (classIndicator) {
			scaleClassIndicator ();
		}
	}

	void scaleClassIndicator ()
	{
		classIndicator.localScale = Vector3.one * (Camera.main.orthographicSize * 0.25f);
		classIndicator.localPosition = Vector3.zero + Vector3.up * (Camera.main.orthographicSize * 0.25f);
	}

	void LateUpdate ()
	{
		transform.GetChild (0).rotation = Quaternion.LookRotation (-Camera.main.transform.forward, Vector3.up);
	}

	void OnCollisionEnter (Collision col)
	{
		Minion otherMinion = col.gameObject.GetComponent<Minion> ();
		if (otherMinion) {
			hitMinion (otherMinion, col);
		} else if (!col.gameObject.CompareTag("Floor")) {
			hitEnvironment (col);
		}
	}

	void hitMinion (Minion otherMinion, Collision col)
	{
		StateController stateControl = GetComponent<StateController> ();
		if (stateControl && stateControl.mill && mill.active) {
			mill.interactionChoice (otherMinion.mill, col);
		} else if (!stateControl && mill.active) {
			mill.interactionChoice (otherMinion.mill, col);
		}
		if (belief.canConvert) {
			belief.conversionCheck (otherMinion.belief);
		}
	}

	void hitEnvironment (Collision col)
	{
		StateController stateControl = GetComponent<StateController> ();
		if (stateControl && stateControl.mill) {
			mill.rb.velocity = mill.getRandomFlatDirection ();
		} else if (!stateControl && mill.active) {
			mill.rb.velocity = mill.getRandomFlatDirection ();
		}
	}
}
