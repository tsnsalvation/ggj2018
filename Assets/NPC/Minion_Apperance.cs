﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minion_Apperance : MonoBehaviour {
    public bool male;
    public GameObject maleCutout;
    public GameObject femaleCutout;

    public int firstName;
    public int middleName;
    public int lastName;
    
    public int backStory;

    void Start () {
        float rand = Random.Range(0.0f, 1.0f);
        if(rand >= 0.5f)
        {
            male = true;
        }
        else
        {
            male = false;
        }

        if(male)
        {
            if (maleCutout != null && femaleCutout != null)
            {
                maleCutout.SetActive(true);
                femaleCutout.SetActive(false);
            }
        }
        else
        {
            if (maleCutout != null && femaleCutout != null)
            {
                femaleCutout.SetActive(true);
                maleCutout.SetActive(false);
            }
        }
        if (male)
        {
            firstName = Random.Range(0, GameManager.instance.npcProfiles.maleFirstNames.Count);
            middleName = Random.Range(0, GameManager.instance.npcProfiles.maleMiddleNames.Count);
            lastName = Random.Range(0, GameManager.instance.npcProfiles.maleLastNames.Count);
        }
        else
        {
            firstName = Random.Range(0, GameManager.instance.npcProfiles.femaleFirstNames.Count);
            middleName = Random.Range(0, GameManager.instance.npcProfiles.femaleMiddleNames.Count);
            lastName = Random.Range(0, GameManager.instance.npcProfiles.femaleLastNames.Count);
        }
	}
}
