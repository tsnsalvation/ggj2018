﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Minion_Settings : MonoBehaviour {

	public MillSetting mill = new MillSetting ();
	public BeliefSetting belief = new BeliefSetting ();

	[System.Serializable]
	public class MillSetting {
		
		[Range(0f, 100f)]
		public float bounceChance = 40f;
		[Range(0f, 100f)]
		public float talkChance = 20f;
		[Range(0f, 100f)]
		public float groupUpChance = 40f;

		public float moveSpeed = 5f;
		public float talkTime = 2f;
		public float groupTime = 2f;

		[HideInInspector]
		public Material dbgWalk, dbgTalk, dbgGroup;

		public void normaliseInteractionChances ()
		{
			float total = bounceChance + talkChance + groupUpChance;
			float normalise = 100f / total;
			bounceChance *= normalise;
			talkChance *= normalise;
			groupUpChance *= normalise;
		}
	}

	[System.Serializable]
	public class BeliefSetting {

		public float beliefVariance = 0.5f;
		public float passingModifier = 0.5f;
		public float talkingModifier = 2f;

		public float force = 1f;
		public float range = 1f;
		public float delay = 1f;

		public bool decay = true;
		public float decayRate = 1f;

		[HideInInspector]
		public Material dbgNeutral, dbgHeretic, dbgPious;

	}

	void Start ()
	{
		setMaterial ();
		gameObject.tag = "Minion";
		GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;

		if (transform.childCount ==0){
			GameObject gimbal = new GameObject ();
			gimbal.transform.parent = transform;
			gimbal.name = "Gimbal";
			gimbal.transform.localPosition = Vector3.zero;
			gimbal.transform.localRotation = Quaternion.identity;
		}
	}

	void Update ()
	{
		mill.normaliseInteractionChances ();

		Minion control = GetComponent<Minion> ();
		if (!control.settings) {
			control.settings = GetComponent<Minion_Settings> ();
		}
		if (!control.mill) {
			control.mill = GetComponent<Minion_Mill> ();
		}
		if (!control.belief) {
			control.belief = GetComponent<Minion_Belief> ();
		}
		control.mill.applySettings (this.mill);
		control.belief.applySettings (this.belief);
	}

	public void setMaterial ()
	{
		mill.dbgWalk = new Material (Shader.Find ("Standard"));
		mill.dbgTalk = new Material (mill.dbgWalk);
		mill.dbgTalk.color = Color.green;
		mill.dbgGroup = new Material (mill.dbgWalk);
		mill.dbgGroup.color = Color.blue;

		belief.dbgNeutral = new Material (Shader.Find ("Standard"));
		belief.dbgHeretic = new Material (belief.dbgNeutral);
		belief.dbgHeretic.color = Color.red;
		belief.dbgPious = new Material (belief.dbgNeutral);
		belief.dbgPious.color = Color.yellow;
	}



}
